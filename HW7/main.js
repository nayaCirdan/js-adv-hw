import Film from "./Film.js";
const mainContainer = document.querySelector('.container');

let xhr = new XMLHttpRequest();
xhr.open('GET', 'https://swapi.dev/api/films/');
xhr.responseType = 'json';
xhr.send();

const axiosGetFilms=axios.get('https://swapi.dev/api/films/');
axiosGetFilms.then(response=>{
        const filmsArr=response.data.results;
        mainContainer.innerHTML='';

        filmsArr.forEach((film)=>{

            const charactersArr=film.characters;
            const filmTitle=film.title;

            const filmSW=new Film(filmTitle, charactersArr, mainContainer);

        })
    })
