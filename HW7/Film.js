export default class Film {
    constructor(filmTitle, charactersArr, mainContainer) {
        this._title = filmTitle;
        this._characters = charactersArr;
        this._mainContainer = mainContainer;
        this.render();
    }

    render() {
        const filmHtml = document.createElement('div');
        filmHtml.classList.add('film-item')
        filmHtml.innerHTML = `<div class="film-title">${this._title}</div>`
        this._mainContainer.append(filmHtml);

        const charInfo = document.createElement('ul');
        charInfo.classList.add('char-list');
        filmHtml.append(charInfo);

        this.getCharacters(charInfo);
    }

    getCharacters(insideContainer) {
        let promisesArr = [];
            promisesArr = this._characters.map(char => axios.get(char));


            Promise.all(promisesArr).then((responses) => {

                responses.forEach((response, index)=>{

                    const charItem = document.createElement('li');
                    charItem.classList.add('char-item');
                    charItem.innerHTML = response.data.name;
                    insideContainer.insertAdjacentElement('beforeend', charItem);
                })


            });

    };
}




