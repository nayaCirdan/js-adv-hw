class createGame {
    constructor() {
        this.targetCell=null;
        this.counterHuman=0;
        this.counterComputer=0;
        this.btnStart=document.querySelector('.start-btn');
        this.level=null;
        this.timerValue=null;
        this.btnStart.addEventListener('click', this.chooseLevel.bind(this));

    }
    startGame(level) {

        if (this.targetCell) {
            this.targetCell.removeEventListener('click', this.userClickHandler.bind(this));
        }
        if(this.counterHuman===50){
            alert(`Human won. your score ${this.counterHuman}, computer score${this.counterComputer}`);

        } else if (this.counterComputer===50) {
            alert(`Computer won. your score ${this.counterHuman}, computer score${this.counterComputer}`)
        } else{ if (this.targetCell) {
            this.targetCell.removeEventListener('click', this.userClickHandler.bind(this));
        }

            this.getRandomCell();
            this.activateCell(level);
        }
    }

    getRandomCell() {

        this.cells=document.querySelectorAll('.cell:not(.cell-computer):not(.cell-human)');
        let randomIndex=Math.floor(Math.random()*this.cells.length);
        this.targetCell= this.cells[randomIndex];
    }
    activateCell(level) {

        this.activeTimer=level;
        this.targetCell.classList.add('cell-try');
        this.targetCell.addEventListener('click', this.userClickHandler.bind(this));
        this.timer=setTimeout(this.computerHandler.bind(this), this.activeTimer);

    }
    computerHandler() {
        if(this.targetCell.classList.contains('cell-try')) {
            this.targetCell.classList.remove('cell-try');
            this.targetCell.classList.add('cell-computer');
            this.counterComputer++;
            this.targetCell.removeEventListener('click', this.userClickHandler.bind(this));
            this.startGame(this.level);
        }

    }
    userClickHandler() {
        this.targetCell.classList.remove('cell-try');
        this.targetCell.classList.add('cell-human');
        this.counterHuman++;
        clearTimeout(this.timer);
        this.targetCell.removeEventListener('click', this.userClickHandler.bind(this));
        this.startGame(this.level);

    }
    chooseLevel(){
        let checkedInput=document.querySelector('.input-level:checked');
        this.level = (+checkedInput.value);

        this.startGame(this.level);
        }
}

new createGame();