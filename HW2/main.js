
class HamburgerException extends Error  {
    constructor(message) {
        super(message);
        this.name = "Hamburger Exception";
        this.message = message;
    }
}



class Hamburger {

    constructor(size, stuffing) {
        try {
            if (!arguments.length) {
                throw new HamburgerException("Вы должны выбрать размер гамбургера: Hamburger.SIZE_SMALL или Hamburger.SIZE_LARGE");
            }
            if (arguments.length === 1) {
                throw new HamburgerException("Вы должны выбрать начинку для гамбургера: Hamburger.STUFFING_CHEESE, Hamburger.STUFFING_SALAD или Hamburger.STUFFING_POTATO");
            }
            if (size !== Hamburger.SIZE_SMALL && size !== Hamburger.SIZE_LARGE) {
                throw new HamburgerException("Неправильно указан размер, возможные значения: Hamburger.SIZE_SMALL или Hamburger.SIZE_LARGE");
            }
            if (stuffing !== Hamburger.STUFFING_CHEESE && stuffing !== Hamburger.STUFFING_SALAD && stuffing !== Hamburger.STUFFING_POTATO) {
                throw new HamburgerException(`Неправильно указано название начинки, выберите Hamburger.STUFFING_CHEESE или Hamburger.STUFFING_SALAD или Hamburger.STUFFING_POTATO`);
            }

            this._size = size;
            this._stuffing = stuffing;
            this._toppings = [];

        } catch (error) {
            console.log(error.message);
        }
    }

    /* Размеры, виды начинок и добавок */
    static SIZE_SMALL = {type: 'Маленький размер',price:50, cal:20};
    static SIZE_LARGE = {type: 'Большой размер',price:100, cal:40};
    static STUFFING_CHEESE = {type: 'Начинка сырочек',price:10, cal:20};
    static STUFFING_SALAD = {type: 'Начинка салатик',price:20, cal:5};
    static STUFFING_POTATO = {type: 'Начинка картошечка',price:15, cal:10};
    static TOPPING_MAYO = {type: 'Добавка мазик',price:20, cal:5};
    static TOPPING_SPICE = {type: 'Добавка приправка',price:15, cal:0};



    addTopping (topping) {
        try {
            if (!arguments.length) {
                throw new HamburgerException('Вы не ввели название топпинга')
            }
            if (topping !== Hamburger.TOPPING_MAYO && topping !== Hamburger.TOPPING_SPICE ) {
                throw new HamburgerException(`Неправильное название топпинга. Выберите Hamburger.TOPPING_MAYO или Hamburger.TOPPING_SPICE`)
            }
            if (this._toppings.length>0){
                if (this._toppings.includes(topping)) {
                    throw new HamburgerException('Такой топпинг уже есть, выберите другой');
                }
                else {
                    this._toppings.push(topping);
                }
            } else {
                this._toppings.push(topping);
            }
            if (arguments.length>1) {
                throw new HamburgerException('Пожалуйста вводите топпинги по одному ')
            }
        } catch (error) {
            console.log(error.message);
        }
    };


    removeTopping (topping) {
        try {
            if (!arguments.length) {
                throw new HamburgerException('Вы не ввели название топпинга')
            }
            if (topping!==Hamburger.TOPPING_MAYO && topping!==Hamburger.TOPPING_SPICE) {
                throw new HamburgerException(`Неправильное название топпинга. Выберите Hamburger.TOPPING_MAYO или Hamburger.TOPPING_SPICE`)
            }
            if (this._toppings.length>0){
                if (this._toppings.includes(topping)) {
                    var toppingPos= this._toppings.indexOf(topping);
                    this._toppings.splice(toppingPos,1);
                }
                else {
                    throw new HamburgerException(`Топпинга ${topping} не было в списке топпингов`);
                }
            } else {
                throw new HamburgerException('Пока не было добавлено ни одного топпинга');
            }
            if (arguments.length>1) {
                throw new HamburgerException('Пожалуйста вводите топпинги по одному ')
            }
        } catch (error) {
            console.log(error.message);
        }
    };



    getToppings() {
        return this._toppings;
    };


    getSize() {
        return this._size;
    };

    getStuffing() {
        return this._stuffing;
    };


    calculatePrice() {
        let hamburgerPrice = this._size.price + this._stuffing.price;
        if (this._toppings.length) {
            hamburgerPrice += this._toppings.reduce((sum, topping) => {
                return sum + topping.price;
            }, 0);
        }
        return hamburgerPrice;
    };


    calculateCalories() {
        let hamburgerCal = this._size.cal + this._stuffing.cal;
        if (this._toppings.length) {
            hamburgerCal += this._toppings.reduce((calSum, topping) => {
                return calSum + topping.cal;
            }, 0);
        }
        return hamburgerCal;
    };

}


//Проверка
const hamburger = new Hamburger(Hamburger.SIZE_SMALL, Hamburger.STUFFING_CHEESE);

hamburger.addTopping(Hamburger.TOPPING_MAYO);
console.log('Пытаемся добавить тот же топпинг еще раз');
hamburger.addTopping(Hamburger.TOPPING_MAYO);
console.log("Calories: %f", hamburger.calculateCalories());
console.log('Узнаем  цену');
console.log("Price: %f", hamburger.calculatePrice());
console.log(hamburger);
console.log('Добавляем еще одну приправу');
hamburger.addTopping(Hamburger.TOPPING_SPICE);
console.log('Узнаем новую цену');
console.log("Price with sauce: %f", hamburger.calculatePrice());
console.log(hamburger);
console.log("Is hamburger large: %s", hamburger.getSize() === Hamburger.SIZE_LARGE);
console.log('Убрали добавку');
hamburger.removeTopping(Hamburger.TOPPING_SPICE);
console.log("Have %d toppings", hamburger.getToppings().length);


const h2 = new Hamburger(); // => HamburgerException: no size given
const h3 = new Hamburger(Hamburger.TOPPING_SPICE, Hamburger.TOPPING_SPICE);
// => HamburgerException: invalid size 'TOPPING_SAUCE'
const h4 = new Hamburger(Hamburger.SIZE_SMALL, Hamburger.TOPPING_SPICE);

