import Film from "./Film.js";
const mainContainer = document.querySelector('.container');

let xhr = new XMLHttpRequest();
xhr.open('GET', 'https://swapi.dev/api/films/');
xhr.responseType = 'json';
xhr.send();

const preloader=`<div class="lds-ring"><div></div><div></div><div></div><div></div></div>`;

xhr.onload=function () {
    const filmsArr=xhr.response.results;

    mainContainer.innerHTML='';
    filmsArr.forEach((film)=>{

        const charactersArr=film.characters;
        const filmTitle=film.title;

        const filmSW=new Film(filmTitle, charactersArr, mainContainer);

    })
}

xhr.onprogress = function(event) {
   mainContainer.innerHTML=preloader;

};