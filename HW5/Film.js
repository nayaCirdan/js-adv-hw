export default class Film {
    constructor(filmTitle, charactersArr, mainContainer) {
        this._title = filmTitle;
        this._characters = charactersArr;
        this._mainContainer=mainContainer;
        this.render();
    }

    render () {
        console.log(this._title);
        const filmHtml=document.createElement('div');
        filmHtml.classList.add('film-item')
        filmHtml.innerHTML=`<div class="film-title">${this._title}</div>`
        this._mainContainer.append(filmHtml);

        const charInfo=document.createElement('ul');
        charInfo.classList.add('char-list');
        filmHtml.append(charInfo);

        this.getCharacters(charInfo);
    }

    getCharacters(insideContainer){
        this._characters.forEach((char)=>{
            let insidRequest=new XMLHttpRequest();
            insidRequest.open('GET', char);
            insidRequest.responseType='json';
            insidRequest.send();

            insidRequest.onload=function () {
                const insideResponse=insidRequest.response;

                for(let char in insideResponse) {

                   if (char==='name'){
                       const charItem=document.createElement('li');
                       charItem.classList.add('char-item');
                       charItem.innerHTML=insideResponse[char];

                       insideContainer.insertAdjacentElement('beforeend', charItem);
                   }

                }

            }
        })
    }

}

