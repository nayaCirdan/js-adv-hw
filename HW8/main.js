const mainContainer = document.querySelector('.container');

const btn=document.createElement('button');
btn.classList.add('ajax-btn');
btn.innerText='Find me by IP';
mainContainer.appendChild(btn);


const info=document.createElement('div');
info.classList.add('info');



async function find(){
    const ipResult=await (axios.get('https://api.ipify.org/?format=json'));
    const clearIP=ipResult.data.ip;
    const physAdressRedult=await (axios.get(`http://ip-api.com/json/${clearIP}?lang=ru&fields=continent,country,regionNamex,city,district`))

    const {city, continent, country, district} =physAdressRedult.data;

    info.innerHTML=`
    <ul>
    <li>Continent: ${continent};</li>
    <li>Country: ${country};</li>
    <li>City: ${city};</li>
    <li>District: ${district||' No District Found'};</li>
    </ul>
    `;

    mainContainer.appendChild(info);
}

btn.addEventListener('click', find);

